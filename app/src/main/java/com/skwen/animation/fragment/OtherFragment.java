package com.skwen.animation.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.skwen.animation.R;
import com.skwen.animation.databinding.FragmentOtherBinding;

public class OtherFragment extends Fragment {


    private FragmentOtherBinding mBinding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_other, container, false);
        return mBinding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.circlePointView.setColor(Color.BLUE);
        mBinding.circlePointView.setRadius(20f);
        mBinding.startBtn.setOnClickListener(v -> mBinding.circlePointView.startAnimation());
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding.circlePointView.stopAnimation();
    }
}
