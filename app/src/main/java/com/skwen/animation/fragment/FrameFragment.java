package com.skwen.animation.fragment;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.skwen.animation.R;
import com.skwen.animation.databinding.FragmentFrameBinding;

public class FrameFragment extends Fragment {


    private FragmentFrameBinding mBinding;

    private boolean isStart;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_frame, container, false);
        return mBinding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        AnimationDrawable animationDrawable = (AnimationDrawable) mBinding.frameImage.getBackground();

        mBinding.frameStart.setOnClickListener(v -> {
            if (isStart) {
                isStart = false;
                animationDrawable.stop();
                mBinding.frameStart.setText(R.string.tv_start);
            } else {
                isStart = true;
                animationDrawable.start();
                mBinding.frameStart.setText(R.string.tv_stop);
            }
        });

    }
}
