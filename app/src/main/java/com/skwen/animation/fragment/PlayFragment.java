package com.skwen.animation.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.skwen.animation.R;
import com.skwen.animation.databinding.FragmentPlayBinding;

public class PlayFragment extends Fragment {

    private FragmentPlayBinding mBinding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_play, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBinding.rootView.setOnClickListener(v -> {
            mBinding.giftView.addImageView();
        });

        mBinding.douYin.setOnClickListener(v -> {
            mBinding.kuaiShouView.setVisibility(View.GONE);
            mBinding.douYinView.setVisibility(View.VISIBLE);
        });
        mBinding.kuaiShou.setOnClickListener(v -> {
            mBinding.kuaiShouView.setVisibility(View.VISIBLE);
            mBinding.douYinView.setVisibility(View.GONE);
        });

        mBinding.giftBtn.setOnClickListener(v -> {
            mBinding.kuaiShouView.setVisibility(View.GONE);
            mBinding.douYinView.setVisibility(View.GONE);
        });
    }
}
