package com.skwen.animation.fragment;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.skwen.animation.R;
import com.skwen.animation.databinding.FragmentPropertyBinding;

public class PropertyFragment extends Fragment {

    private FragmentPropertyBinding mBinding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_property, container, false);
        return mBinding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        //渐变
        ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(mBinding.bottomRightImg, "alpha", 1f, 0.2f);
        alphaAnimator.setDuration(2000);
        alphaAnimator.setRepeatCount(ValueAnimator.INFINITE);
        alphaAnimator.setRepeatMode(ValueAnimator.REVERSE);
        alphaAnimator.start();

        //X轴平移，Y轴平移使用translationX属性即可。同时XY平移，使用AnimatorSet包含两个属性动画一起即可。
        ObjectAnimator translateAnimator = ObjectAnimator.ofFloat(mBinding.topLeftImg, "translationX", 0f, 300f);
        translateAnimator.setDuration(2000);
        translateAnimator.setRepeatCount(ValueAnimator.INFINITE);
        translateAnimator.setRepeatMode(ValueAnimator.REVERSE);
        translateAnimator.start();

        //缩放大小
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(mBinding.topRightImg, "scaleX", 0.5f, 2f);
        scaleXAnimator.setDuration(2000);
        scaleXAnimator.setRepeatCount(ValueAnimator.INFINITE);
        scaleXAnimator.setRepeatMode(ValueAnimator.REVERSE);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(mBinding.topRightImg, "scaleY", 0.5f, 2f);
        scaleYAnimator.setDuration(2000);
        scaleYAnimator.setRepeatCount(ValueAnimator.INFINITE);
        scaleYAnimator.setRepeatMode(ValueAnimator.REVERSE);
        AnimatorSet scaleAnimatorSet = new AnimatorSet();
        //或者使用：scaleAnimatorSet.play(scaleXAnimator).with(scaleYAnimator);同下
        scaleAnimatorSet.playTogether(scaleXAnimator, scaleYAnimator);
        scaleAnimatorSet.start();

        //旋转动画
        ObjectAnimator rotationAnimator = ObjectAnimator.ofFloat(mBinding.bottomLeftImg, "rotation", 0f, 360f);
        rotationAnimator.setDuration(2000);
        rotationAnimator.setRepeatCount(ValueAnimator.INFINITE);
        rotationAnimator.setRepeatMode(ValueAnimator.REVERSE);
        rotationAnimator.start();

        //组合动画accelerateDecelerateInterpolator
        ObjectAnimator alphaSetAnimator = ObjectAnimator.ofFloat(mBinding.middleImg, "alpha", 1f, 0.2f);
        alphaSetAnimator.setDuration(4000);
        alphaSetAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        alphaSetAnimator.setRepeatCount(ValueAnimator.INFINITE);
        alphaSetAnimator.setRepeatMode(ValueAnimator.REVERSE);
        ObjectAnimator translateXSetAnimator = ObjectAnimator.ofFloat(mBinding.middleImg, "translationX", 0f, 300f);
        translateXSetAnimator.setDuration(2000);
        translateXSetAnimator.setRepeatCount(ValueAnimator.INFINITE);
        translateXSetAnimator.setRepeatMode(ValueAnimator.REVERSE);
        ObjectAnimator translateYSetAnimator = ObjectAnimator.ofFloat(mBinding.middleImg, "translationY", 0f, 300f);
        translateYSetAnimator.setDuration(2000);
        translateYSetAnimator.setRepeatCount(ValueAnimator.INFINITE);
        translateYSetAnimator.setRepeatMode(ValueAnimator.REVERSE);
        ObjectAnimator scaleXSetAnimator = ObjectAnimator.ofFloat(mBinding.middleImg, "scaleX", 0.5f, 2f);
        scaleXSetAnimator.setDuration(2000);
        scaleXSetAnimator.setRepeatCount(ValueAnimator.INFINITE);
        scaleXSetAnimator.setRepeatMode(ValueAnimator.REVERSE);
        ObjectAnimator scaleYSetAnimator = ObjectAnimator.ofFloat(mBinding.middleImg, "scaleY", 0.5f, 2f);
        scaleYSetAnimator.setDuration(2000);
        scaleYSetAnimator.setRepeatCount(ValueAnimator.INFINITE);
        scaleYSetAnimator.setRepeatMode(ValueAnimator.REVERSE);
        ObjectAnimator rotationSetAnimator = ObjectAnimator.ofFloat(mBinding.middleImg, "rotation", 0f, 1080);
        rotationSetAnimator.setDuration(2000);
        rotationSetAnimator.setRepeatCount(ValueAnimator.INFINITE);
        rotationSetAnimator.setRepeatMode(ValueAnimator.REVERSE);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(alphaSetAnimator, translateXSetAnimator, translateYSetAnimator, scaleXSetAnimator, scaleYSetAnimator, rotationSetAnimator);
        animatorSet.start();

    }

}
