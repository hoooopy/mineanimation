package com.skwen.animation.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.skwen.animation.R;
import com.skwen.animation.databinding.FragmentTweenBinding;

public class TweenFragment extends Fragment {


    private FragmentTweenBinding mBinding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_tween, container, false);
        return mBinding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        Animation animationTranslate = AnimationUtils.loadAnimation(getContext(), R.anim.translate_animation);
        mBinding.topLeftImg.startAnimation(animationTranslate);

        Animation animationScale = AnimationUtils.loadAnimation(getContext(), R.anim.scale_animation);
        mBinding.topRightImg.startAnimation(animationScale);

        Animation animationTween = AnimationUtils.loadAnimation(getContext(), R.anim.tween_animation);
        mBinding.middleImg.startAnimation(animationTween);

        Animation animationRotate = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_animation);
        mBinding.bottomLeftImg.startAnimation(animationRotate);

        Animation animationAlpha = AnimationUtils.loadAnimation(getContext(), R.anim.alpha_animation);
        mBinding.bottomRightImg.startAnimation(animationAlpha);
    }


}
