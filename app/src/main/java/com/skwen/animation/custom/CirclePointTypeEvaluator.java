package com.skwen.animation.custom;

import android.animation.TypeEvaluator;
import android.graphics.PointF;
import android.util.Log;

public class CirclePointTypeEvaluator implements TypeEvaluator<PointF> {

    /**
     * CirclePointTypeEvaluator 继承了TypeEvaluator类，并实现了他唯一的方法evaluate；
     * 这里我们的逻辑很简单，x的值随着fraction 不断变化，并最终达到结束值；
     * y的值就是当前x值所对应的sin(x) 值，然后用x 和 y 产生一个新的点（Point对象）返回。
     *
     * @param fraction   代表当前动画完成的百分比；
     * @param startValue 动画的初始值
     * @param endValue   动画的结束值
     * @return 点坐标
     */
    @Override
    public PointF evaluate(float fraction, PointF startValue, PointF endValue) {
        float x = startValue.x + fraction * (endValue.x - startValue.x);

        float y = (float) (Math.sin(x * Math.PI / 180) * 100) + endValue.y / 2;

        Log.d("point", "x:" + x + "  y:" + y);
        return new PointF(x, y);
    }
}
