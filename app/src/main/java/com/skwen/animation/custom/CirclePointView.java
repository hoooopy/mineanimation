package com.skwen.animation.custom;

import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import androidx.annotation.Nullable;

public class CirclePointView extends View {

    private Paint mPointPaint, mCirclePaint;

    private PointF mCurrentPoint;

    public static final int RADIUS = 20;

    private float radius = 20;

    private int color;

    private AnimatorSet animSet;

    public CirclePointView(Context context) {
        super(context);
        init();
    }

    public CirclePointView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CirclePointView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mPointPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPointPaint.setStrokeWidth(8f);
        mPointPaint.setColor(Color.BLACK);

        mCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCirclePaint.setColor(Color.TRANSPARENT);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        if (mCurrentPoint == null) {
            mCurrentPoint = new PointF(RADIUS, RADIUS);
        }
        canvas.drawCircle(mCurrentPoint.x, mCurrentPoint.y, radius, mCirclePaint);
        drawLine(canvas);
    }

    private void drawLine(Canvas canvas) {
        canvas.drawLine(10, getHeight() / 2, getWidth(), getHeight() / 2, mPointPaint);
        canvas.drawLine(10, getHeight() / 2 - 150, 10, getHeight() / 2 + 150, mPointPaint);
        canvas.drawPoint(mCurrentPoint.x, mCurrentPoint.y, mPointPaint);
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
        mCirclePaint.setColor(this.color);
    }

    public void startAnimation() {
        PointF startP = new PointF(RADIUS, RADIUS);
        PointF endP = new PointF(getWidth() - RADIUS, getHeight() - RADIUS);
        final ValueAnimator valueAnimator = ValueAnimator.ofObject(new CirclePointTypeEvaluator(), startP, endP);
        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
        valueAnimator.setRepeatMode(ValueAnimator.REVERSE);
        valueAnimator.addUpdateListener(animation -> {
            mCurrentPoint = (PointF) animation.getAnimatedValue();
            postInvalidate();
        });

        ObjectAnimator animColor = ObjectAnimator.ofObject(this, "color", new ArgbEvaluator(), Color.GREEN,
                Color.YELLOW, Color.BLUE, Color.WHITE, Color.RED);
        animColor.setRepeatCount(ValueAnimator.INFINITE);
        animColor.setRepeatMode(ValueAnimator.REVERSE);

        ObjectAnimator animScale = ObjectAnimator.ofFloat(this, "radius", 10f, 20f, 30f, 40f, 30f, 20f, 10f);
        animScale.setRepeatCount(ValueAnimator.INFINITE);
        animScale.setRepeatMode(ValueAnimator.REVERSE);
        animScale.setDuration(5000);

        animSet = new AnimatorSet();
        animSet.play(valueAnimator).with(animColor).with(animScale);
        animSet.setDuration(5000);
        animSet.setInterpolator(new DecelerateInterpolator());
        animSet.start();
    }

    public void pauseAnimation() {
        if (animSet != null) {
            animSet.pause();
        }
    }


    public void stopAnimation() {
        if (animSet != null) {
            animSet.cancel();
            this.clearAnimation();
        }
    }
}
