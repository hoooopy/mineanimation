package com.skwen.animation.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.skwen.animation.R;

public class KuaiShouView extends ConstraintLayout {


    private Drawable[] drawables = new Drawable[4];

    float downX, downY;

    long downTime;

    public KuaiShouView(@NonNull Context context) {
        super(context);
        init();
    }

    public KuaiShouView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        drawables[0] = ContextCompat.getDrawable(getContext(), R.drawable.ic_baseline_favorite_blue_6);
        drawables[1] = ContextCompat.getDrawable(getContext(), R.drawable.ic_baseline_favorite_green_6);
        drawables[2] = ContextCompat.getDrawable(getContext(), R.drawable.ic_baseline_favorite_purple_6);
        drawables[3] = ContextCompat.getDrawable(getContext(), R.drawable.ic_baseline_favorite_yellow_6);
    }


    public void addLoveView(float x, float y) {

        for (int i = 0; i < 4; i++) {
            ImageView smallImageView = new ImageView(getContext());
            smallImageView.setImageDrawable(drawables[(int) (Math.random() * 4)]);
            smallImageView.setRotation((float) (Math.random() * 30));
            float pX = x + (float) Math.random() * 200;
            float pY = y + (float) Math.random() * 100;
            smallImageView.setX(pX);
            smallImageView.setY(pY);
            addView(smallImageView, 100, 100);
            getSmallRightValueAnimator(smallImageView, pX, pY).start();
        }

        for (int i = 0; i < 4; i++) {
            ImageView smallImageView = new ImageView(getContext());
            smallImageView.setImageDrawable(drawables[(int) (Math.random() * 4)]);
            smallImageView.setRotation((float) (Math.random() * 30));
            float pX = x + (float) Math.random() * 200;
            float pY = y + (float) Math.random() * 100;
            smallImageView.setX(pX);
            smallImageView.setY(pY);
            addView(smallImageView, 100, 100);
            getSmallLeftValueAnimator(smallImageView, pX, pY).start();
        }


        ImageView imageView = new ImageView(getContext());
        imageView.setImageResource(R.drawable.ic_baseline_favorite_24);
        imageView.setRotation((float) (Math.random() * 10));
        imageView.setX(x);
        imageView.setY(y);
        addView(imageView, 300, 300);
        bigViewAnimator(imageView).start();

    }

    private Animator bigViewAnimator(ImageView imageView) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(imageView, View.ALPHA, 1f, 0f);
        objectAnimator.setDuration(600);
        objectAnimator.setInterpolator(new LinearInterpolator());
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imageView, View.SCALE_X, 1f, 1.3f);
        scaleXAnimator.setDuration(500);
        scaleXAnimator.setInterpolator(new LinearInterpolator());
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imageView, View.SCALE_Y, 1f, 1.3f);
        scaleYAnimator.setDuration(500);
        scaleYAnimator.setInterpolator(new LinearInterpolator());
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, scaleXAnimator, scaleYAnimator);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                removeView(imageView);
            }
        });
        return animatorSet;
    }

    private AnimatorSet getSmallLeftValueAnimator(View target, float x, float y) {


        PointF pointF3 = new PointF();
        pointF3.x = x + (float) (Math.random() * 400);
        pointF3.y = y - (float) (Math.random() * 500);

        PointF pointF4 = new PointF();
        pointF4.x = x + (float) (Math.random() * 400);
        pointF4.y = y - (float) (Math.random() * 500);

        //初始化一个贝塞尔计算器- - 传入
        BezierEvaluator evaluator2 = new BezierEvaluator(pointF3, pointF4);

        //这里最好画个图 理解一下 传入了起点 和 终点
        PointF randomEndPoint = new PointF(x, y - 500);
        ValueAnimator animator2 = ValueAnimator.ofObject(evaluator2, new PointF(x, y), randomEndPoint);
        animator2.addUpdateListener(new BezierListener(target));
        animator2.setTarget(target);
        animator2.setDuration(1000);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animator2);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                removeView(target);
            }
        });
        return animatorSet;
    }


    private AnimatorSet getSmallRightValueAnimator(View target, float x, float y) {

        PointF pointF = new PointF();
        pointF.x = x - (float) (Math.random() * 400);
        pointF.y = y - (float) (Math.random() * 500);

        PointF pointF2 = new PointF();
        pointF2.x = x - (float) (Math.random() * 400);
        pointF2.y = y - (float) (Math.random() * 500);

        //初始化一个贝塞尔计算器- - 传入
        BezierEvaluator evaluator = new BezierEvaluator(pointF, pointF2);

        //这里最好画个图 理解一下 传入了起点 和 终点
        PointF randomEndPoint = new PointF(x, y - 500);
        ValueAnimator animator = ValueAnimator.ofObject(evaluator, new PointF(x, y), randomEndPoint);
        animator.addUpdateListener(new BezierListener(target));
        animator.setTarget(target);
        animator.setDuration(1000);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animator);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                removeView(target);
            }
        });
        return animatorSet;
    }

    public static class BezierEvaluator implements TypeEvaluator<PointF> {


        private PointF pointF1;
        private PointF pointF2;

        public BezierEvaluator(PointF pointF1, PointF pointF2) {
            this.pointF1 = pointF1;
            this.pointF2 = pointF2;
        }

        @Override
        public PointF evaluate(float time, PointF startValue,
                               PointF endValue) {

            float timeLeft = 1.0f - time;
            PointF point = new PointF();//结果

            point.x = timeLeft * timeLeft * timeLeft * (startValue.x)
                    + 3 * timeLeft * timeLeft * time * (pointF1.x)
                    + 3 * timeLeft * time * time * (pointF2.x)
                    + time * time * time * (endValue.x);

            point.y = timeLeft * timeLeft * timeLeft * (startValue.y)
                    + 3 * timeLeft * timeLeft * time * (pointF1.y)
                    + 3 * timeLeft * time * time * (pointF2.y)
                    + time * time * time * (endValue.y);
            return point;
        }
    }

    private static class BezierListener implements ValueAnimator.AnimatorUpdateListener {

        private View target;

        public BezierListener(View target) {
            this.target = target;
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            //这里获取到贝塞尔曲线计算出来的的x y值 赋值给view 这样就能让爱心随着曲线走啦
            PointF pointF = (PointF) animation.getAnimatedValue();
            target.setX(pointF.x);
            target.setY(pointF.y);
            // 这里顺便做一个alpha动画
            target.setAlpha(1 - animation.getAnimatedFraction());
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        boolean isTouchClick = true;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downTime = System.currentTimeMillis();
                downX = event.getX();
                downY = event.getY();
                Log.d("skwen", "ACTION_DOWN");
                break;
            case MotionEvent.ACTION_MOVE:
                Log.d("skwen", "ACTION_MOVE");
                break;
            case MotionEvent.ACTION_UP:
                Log.d("skwen", "ACTION_UP");
                if (event.getX() - downX >= 10 || event.getY() - downY >= 10 || System.currentTimeMillis() - downTime >= 500) {
                    isTouchClick = false;
                } else {
                    isTouchClick = true;
                    addLoveView(event.getX() - 150, event.getY() - 150);
                }
                break;
        }
        return isTouchClick;
    }
}
