package com.skwen.animation.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Property;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.skwen.animation.R;

public class DouyinView extends ConstraintLayout {


    float downX, downY;

    long downTime;

    public DouyinView(@NonNull Context context) {
        super(context);
        init();
    }

    public DouyinView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
    }


    public void addLoveView(float x, float y) {

        ImageView imageView = new ImageView(getContext());
        imageView.setImageResource(R.drawable.ic_baseline_favorite_24);
        imageView.setRotation((float) (Math.random() * 30));
        imageView.setX(x);
        imageView.setY(y - 300);
        addView(imageView, 300, 300);
        bigViewAnimator(imageView, x, y).start();
    }

    private Animator bigViewAnimator(ImageView imageView, float x, float y) {


        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(imageView, View.ALPHA, 0.7f, 0f);
        objectAnimator.setDuration(700);
        objectAnimator.setInterpolator(new LinearInterpolator());
        ObjectAnimator moveYAnimator = ObjectAnimator.ofFloat(imageView, View.Y, y - 300, y - 600);
        moveYAnimator.setDuration(600);
        moveYAnimator.setInterpolator(new LinearInterpolator());
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(objectAnimator).with(moveYAnimator);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                removeView(imageView);
            }
        });

        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imageView, View.SCALE_X, 1f, 1.3f);
        scaleXAnimator.setDuration(200);
        scaleXAnimator.setInterpolator(new LinearInterpolator());
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imageView, View.SCALE_Y, 1f, 1.3f);
        scaleYAnimator.setDuration(200);
        scaleYAnimator.setInterpolator(new LinearInterpolator());
        ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(imageView, View.ALPHA, 1f, 0.7f);
        alphaAnimator.setDuration(200);
        alphaAnimator.setInterpolator(new LinearInterpolator());

        AnimatorSet scaleSet = new AnimatorSet();
        scaleSet.playTogether(scaleXAnimator, scaleYAnimator, alphaAnimator);
        scaleSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                animatorSet.start();
            }
        });

        return scaleSet;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        boolean isTouchClick = true;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downTime = System.currentTimeMillis();
                downX = event.getX();
                downY = event.getY();
                Log.d("skwen", "ACTION_DOWN");
                break;
            case MotionEvent.ACTION_MOVE:
                Log.d("skwen", "ACTION_MOVE");
                break;
            case MotionEvent.ACTION_UP:
                Log.d("skwen", "ACTION_UP");
                if (event.getX() - downX >= 10 || event.getY() - downY >= 10 || System.currentTimeMillis() - downTime >= 500) {
                    isTouchClick = false;
                } else {
                    isTouchClick = true;
                    addLoveView(event.getX() - 150, event.getY() - 150);
                }
                break;
        }
        return isTouchClick;
    }
}
